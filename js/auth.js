const password = 'plimmeristhebest';
let entry = '';
let x = true;

while(x) {
    if(window.localStorage.getItem('entered') == 'true') {
        x = false;
    } else {
        entry = prompt("Please enter the password")
        if(entry == password) {
            alert("Correct password");
            window.localStorage.setItem('entered', 'true')
            x = false;
        } else {
            alert("Incorrect password. Please try again")
            window.localStorage.setItem('entered', 'false')
        }
    }
}